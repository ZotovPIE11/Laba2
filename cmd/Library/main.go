package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// Читатели
func GetReader(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Get Reader " + vars["id"]))
}

func DeleteReader(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Delete Reader "))
}

func CreateReader(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Create Reader " + vars["id"]))

}
func UpdateReader(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Update Reader " + vars["id"]))
}

// Книги
func GetBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Get Book " + vars["id"]))
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Delete Book "))
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Create Book " + vars["id"]))

}
func UpdateBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Write([]byte("Update Book " + vars["id"]))
}

func main() {
	fmt.Println("HelloWorld!")
	r := mux.NewRouter()
	r.HandleFunc("/users/{id}", GetReader).Methods(http.MethodGet)
	r.HandleFunc("/users", DeleteReader).Methods(http.MethodDelete)
	r.HandleFunc("/users/{id}", CreateReader).Methods(http.MethodPost)
	r.HandleFunc("/users/{id}", UpdateReader).Methods(http.MethodPatch)
	r.HandleFunc("/books/{id}", GetBook).Methods(http.MethodGet)
	r.HandleFunc("/books", DeleteBook).Methods(http.MethodDelete)
	r.HandleFunc("/books/{id}", CreateBook).Methods(http.MethodPost)
	r.HandleFunc("/books/{id}", UpdateBook).Methods(http.MethodPatch)

	log.Fatal(http.ListenAndServe(":8080", r))
}
